package home.manipulatedpdf;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

@RunWith(JUnit4.class)
public class OpenPdfTest {
    @Test
    public void hello() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("hello.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("hello.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PdfReader reader = new PdfReader(input.toURI().toURL());
            // step 1: retrieve the size of the first page and create a document-object
            final Document document = new Document(reader.getPageSize(1));
        ) {
            // step 2: create a writer that listens to the document
            final PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(output));
            // step 3: open the document
            document.open();
            // step 4: replace content
            final PdfContentByte cb = writer.getDirectContent();
            final int numberOfPages = reader.getNumberOfPages();

            for (int pageNumber = 1; pageNumber <= numberOfPages; pageNumber++) {
                document.newPage();
                final PdfImportedPage importedPage = writer.getImportedPage(reader, pageNumber);
                cb.addTemplate(importedPage, 1, 0, 0, 1, 0, 0);

                final byte[] content = reader.getPageContent(pageNumber);
                final String replacedContent = new String(content).replace("Hello World", "HELLO WORLD");
                reader.setPageContent(pageNumber, replacedContent.getBytes(StandardCharsets.UTF_8));

//                BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
//                cb.beginText();
//                cb.setFontAndSize(bf, 14);
//                cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "HELLO", reader.getPageSize(pageNumber).getWidth() / 2, 40, 0);
//                cb.endText();
            }
        }
    }

    @Test
    public void pages() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("pages.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("pages.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PdfReader reader = new PdfReader(input.toURI().toURL());
            // step 1: retrieve the size of the first page and create a document-object
            final Document document = new Document(reader.getPageSize(1));
        ) {
            // step 2: create a writer that listens to the document
            final PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(output));
            // step 3: open the document
            document.open();
            // step 4: replace content
            final PdfContentByte cb = writer.getDirectContent();
            final int numberOfPages = reader.getNumberOfPages();

            for (int pageNumber = 1; pageNumber <= numberOfPages; pageNumber++) {
                document.newPage();
                final PdfImportedPage importedPage = writer.getImportedPage(reader, pageNumber);
                cb.addTemplate(importedPage, 1, 0, 0, 1, 0, 0);

                System.out.println("Page ".concat(String.valueOf(pageNumber)).concat(" :"));
                final String original = new String(reader.getPageContent(pageNumber), StandardCharsets.UTF_8);
                System.out.println("ORIGINAL:\n".concat(original));
                final String replaced = original.replace("Hello World", "HELLO WORLD");
                System.out.println("REPLACED:\n".concat(replaced));
                reader.setPageContent(pageNumber, replaced.getBytes(StandardCharsets.UTF_8));
            }
        }
    }
}
