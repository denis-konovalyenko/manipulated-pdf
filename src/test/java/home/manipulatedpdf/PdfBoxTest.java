package home.manipulatedpdf;

import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.color.PDColor;
import org.apache.pdfbox.pdmodel.graphics.color.PDDeviceRGB;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class PdfBoxTest {
    @Test
    public void hello() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("hello.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("hello.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PDDocument inputDocument = PDDocument.load(input);
            final PDDocument outputDocument = new PDDocument()
        ) {
            for (final PDPage page : inputDocument.getPages()) {
                outputDocument.addPage(page);
                final PDFStreamParser parser = new PDFStreamParser(page);
                parser.parse();
                if (0 == parser.getTokens().size()) {
                    continue;
                }
                final PDStream stream = new PDStream(outputDocument);
                final OutputStream outputStream = stream.createOutputStream(COSName.FLATE_DECODE);
                final ContentStreamWriter writer = new ContentStreamWriter(outputStream);

                for (final Object token : parser.getTokens()) {
                    if (token instanceof COSString) {
                        final String original = ((COSString) token).getString();
                        final String replaced = original.replace("Hello World", "HELLO WORLD");
                        final COSString string = new COSString(replaced);
                        writer.writeToken(string);
                        continue;
                    }
                    writer.writeTokens(token);
                }
                outputStream.close();
                page.setContents(stream);
            }
            outputDocument.save(output);
        }
    }

    @Test
    public void pages() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("pages.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("pages.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PDDocument inputDocument = PDDocument.load(input);
            final PDDocument outputDocument = new PDDocument()
        ) {
            for (final PDPage page : inputDocument.getPages()) {
                outputDocument.addPage(page);
                final PDFStreamParser parser = new PDFStreamParser(page);
                parser.parse();
                if (0 == parser.getTokens().size()) {
                    continue;
                }
                final PDStream stream = new PDStream(outputDocument);
                final OutputStream outputStream = stream.createOutputStream(COSName.FLATE_DECODE);
                final ContentStreamWriter writer = new ContentStreamWriter(outputStream);

                for (final Object token : parser.getTokens()) {
                    if (token instanceof COSString) {
                        final String original = ((COSString) token).getString();
                        final String replaced = original.replace("Hello World", "HELLO WORLD");
                        final COSString string = new COSString(replaced);
                        writer.writeToken(string);
                        continue;
                    }
                    writer.writeTokens(token);
                }
                outputStream.close();
                page.setContents(stream);
            }
            outputDocument.save(output);
        }
    }

    @Test
    public void arabic() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("arabic-4.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("arabic-4.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PDDocument inputDocument = PDDocument.load(input);
            final PDDocument outputDocument = new PDDocument()
        ) {
            for (final PDPage page : inputDocument.getPages()) {
                outputDocument.addPage(page);
                final PDFStreamParser parser = new PDFStreamParser(page);
                parser.parse();
                if (0 == parser.getTokens().size()) {
                    continue;
                }
                final PDStream stream = new PDStream(outputDocument);
                final OutputStream outputStream = stream.createOutputStream(COSName.FLATE_DECODE);
                final ContentStreamWriter writer = new ContentStreamWriter(outputStream);

                for (final Object token : parser.getTokens()) {
                    if (token instanceof COSString) {
                        final String original = ((COSString) token).getString();
                        final String replaced;
                        if ("مرحبا بالعالم".equals(original)) {
                            replaced = "HELLO WORLD";
                        } else if ("Hello World".equals(original)) {
                            replaced = "مرحبا بالعالم";
                        } else {
                            replaced = original;
                        }
                        final COSString string = new COSString(replaced);
                        writer.writeToken(string);
                        continue;
                    }
                    writer.writeTokens(token);
                }
                outputStream.close();
                page.setContents(stream);
            }
            outputDocument.save(output);
        }
    }

    @Test
    public void overridesTextsByRectangles() throws Exception {
        final Path inputPath = Paths.get(this.getClass().getResource("/").toURI());
        final String[] documentsNames = inputPath.toFile().list((d, n) -> n.endsWith(".pdf"));
        Assertions.assertThat(documentsNames).isNotNull();

        final Path outputPath = inputPath.resolve("out").resolve("overridden-by-rectangles");
        outputPath.toFile().mkdirs();
//        outputPath.toFile().deleteOnExit();

        Arrays.stream(documentsNames)
            .sorted()
            .forEach(dn -> {
                overrideTextsByRectangles(
                    inputPath.resolve(dn).toFile(),
                    outputPath.resolve(dn).toFile()
                );
            });
    }

    private void overrideTextsByRectangles(final File input, final File output) {
        try (
            final PDDocument inputDocument = PDDocument.load(input);
            final PDDocument outputDocument = new PDDocument()
        ) {
            final TextLines textLines = new TextLines(
                outputDocument,
                new ExtractionEngine()
            );

            int numberOfPages = inputDocument.getNumberOfPages();
            for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++) {
                final PDPage page = inputDocument.getPage(pageIndex);
                outputDocument.addPage(page);

                for (final TextLine textLine : textLines.fromPage(pageIndex)) {
                    final PDPageContentStream contentStream = new PDPageContentStream(
                        outputDocument,
                        page,
                        PDPageContentStream.AppendMode.APPEND,
                        false,
                        true
                    );

                    contentStream.setNonStrokingColor(new PDColor(new float[]{1f, 0f, 0f}, PDDeviceRGB.INSTANCE));
                    contentStream.addRect(
                        textLine.coordinates().leftX(),
                        textLine.coordinates().lowerY(),
                        textLine.dimensions().width(),
                        textLine.dimensions().height()
                    );
                    contentStream.fill();

                    contentStream.setNonStrokingColor(new PDColor(new float[]{255f, 255f, 255f}, PDDeviceRGB.INSTANCE));
                    contentStream.beginText();
                    contentStream.newLineAtOffset(
                        textLine.coordinates().leftX(),
                        textLine.coordinates().lowerY()
                    );
//                    contentStream.setFont(textLine.font(), textLine.fontSize());
                    contentStream.setFont(PDType1Font.HELVETICA, textLine.fontSize());
                    contentStream.showText("Overridden");
                    contentStream.endText();

                    contentStream.close();
                }
            }
            outputDocument.save(output);
        } catch (IOException e) {
            Assertions.fail(String.format("Error at overriding texts in %s: %s", input.getName(), e.getMessage()));
        }
    }

    @Test
    public void deletesAndWritesNewTexts() throws Exception {
        final Path inputPath = Paths.get(this.getClass().getResource("/").toURI());
        final String[] documentsNames = inputPath.toFile().list((d, n) -> n.endsWith(".pdf"));
        Assertions.assertThat(documentsNames).isNotNull();

        final Path outputPath = inputPath.resolve("out").resolve("deleted-and-written-new");
        outputPath.toFile().mkdirs();
//        outputPath.toFile().deleteOnExit();

        Arrays.stream(documentsNames)
            .sorted()
            .forEach(dn -> {
                deleteAndWriteNewTexts(
                    inputPath.resolve(dn).toFile(),
                    outputPath.resolve(dn).toFile()
                );
            });
    }

    private void deleteAndWriteNewTexts(final File input, final File output) {
        try (
            final PDDocument inputDocument = PDDocument.load(input);
            final PDDocument outputDocument = new PDDocument()
        ) {
            final TextLines textLines = new TextLines(
                outputDocument,
                new ExtractionEngine()
            );

            int numberOfPages = inputDocument.getNumberOfPages();
            for (int pageIndex = 0; pageIndex < numberOfPages; pageIndex++) {
                final PDPage page = inputDocument.getPage(pageIndex);
                outputDocument.addPage(page);
                final List<TextLine> textLineList = textLines.fromPage(pageIndex);

                final PdfContentStreamEditor editor = new PdfContentStreamEditor(outputDocument, page) {
                    final List<String> TEXT_SHOWING_OPERATORS = Arrays.asList("Tj", "'", "\"", "TJ");

                    @Override
                    protected void write(ContentStreamWriter contentStreamWriter, Operator operator, List<COSBase> operands) throws IOException {
                        String operatorString = operator.getName();

                        if (TEXT_SHOWING_OPERATORS.contains(operatorString)) {
                            return;
                        }
                        super.write(contentStreamWriter, operator, operands);
                    }
                };
                editor.processPage(page);

                for (final TextLine textLine : textLineList) {
                    final PDPageContentStream contentStream = new PDPageContentStream(
                        outputDocument,
                        page,
                        PDPageContentStream.AppendMode.APPEND,
                        false,
                        true
                    );

                    contentStream.setNonStrokingColor(new PDColor(new float[]{255f, 0f, 0f}, PDDeviceRGB.INSTANCE));
                    contentStream.beginText();
                    contentStream.newLineAtOffset(
                        textLine.coordinates().leftX(),
                        textLine.coordinates().lowerY()
                    );
//                    contentStream.setFont(textLine.font(), textLine.fontSize());
                    contentStream.setFont(PDType1Font.HELVETICA, textLine.fontSize());
                    contentStream.showText("Deleted and written new");
                    contentStream.endText();

                    contentStream.close();
                }
            }
            outputDocument.save(output);
        } catch (IOException e) {
            Assertions.fail(String.format("Error at deleting and writing new texts in %s: %s", input.getName(), e.getMessage()));
        }
    }
}
//                PDGraphicsState state = getGraphicsState();
//                PDTextState textState = state.getTextState();
//
//                // get the current font
//                PDFont font = textState.getFont();
//                if (font == null)
//                {
//                    LOG.warn("No current font, will use default");
//                    font = PDType1Font.HELVETICA;
//                }
//
//                float fontSize = textState.getFontSize();
//                float horizontalScaling = textState.getHorizontalScaling() / 100f;
//                float charSpacing = textState.getCharacterSpacing();
//
//                // put the text state parameters into matrix form
//                Matrix parameters = new Matrix(
//                        fontSize * horizontalScaling, 0, // 0
//                        0, fontSize,                     // 0
//                        0, textState.getRise());         // 1
