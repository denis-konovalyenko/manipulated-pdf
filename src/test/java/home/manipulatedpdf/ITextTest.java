package home.manipulatedpdf;

import com.itextpdf.kernel.pdf.PdfDictionary;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfName;
import com.itextpdf.kernel.pdf.PdfObject;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfStream;
import com.itextpdf.kernel.pdf.PdfWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

@RunWith(JUnit4.class)
public class ITextTest {
    @Test
    public void hello() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("hello.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("hello.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PdfDocument document = new PdfDocument(
                new PdfReader(input),
                new PdfWriter(output)
            )
        ) {
            final int numberOfPages = document.getNumberOfPages();
            for (int pageNumber = 1; pageNumber <= numberOfPages; pageNumber++) {
                PdfPage page = document.getPage(pageNumber);
                PdfDictionary dict = page.getPdfObject();

                PdfObject object = dict.get(PdfName.Contents);
                if (object instanceof PdfStream) {
                    PdfStream stream = (PdfStream) object;
                    byte[] data = stream.getBytes();
                    String replacedData = new String(data, StandardCharsets.UTF_8).replace("Hello World", "HELLO WORLD");
                    stream.setData(replacedData.getBytes(StandardCharsets.UTF_8));
                }
            }
        }
    }

    @Test
    public void pages() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("pages.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("pages.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PdfDocument document = new PdfDocument(
                new PdfReader(input),
                new PdfWriter(output)
            )
        ) {
            final int numberOfPages = document.getNumberOfPages();
            for (int pageNumber = 1; pageNumber <= numberOfPages; pageNumber++) {
                PdfPage page = document.getPage(pageNumber);
                PdfDictionary dict = page.getPdfObject();

                PdfObject object = dict.get(PdfName.Contents);
                if (object instanceof PdfStream) {
                    PdfStream stream = (PdfStream) object;
                    System.out.println("Page ".concat(String.valueOf(pageNumber)).concat(" :"));
                    byte[] data = stream.getBytes();
                    final String original = new String(data, StandardCharsets.UTF_8);
                    System.out.println("ORIGINAL:\n".concat(original));
                    final String replaced = original.replace("Hello World", "HELLO WORLD");
                    System.out.println("REPLACED:\n".concat(replaced));
                    stream.setData(replaced.getBytes(StandardCharsets.UTF_8));
                }
            }
        }
    }

    @Test
    public void arabic() throws Exception {
        final URL baseUrl = this.getClass().getResource("/");
        final File input = Paths.get(baseUrl.toURI()).resolve("arabic-4.pdf").toFile();
        final File output = Paths.get(baseUrl.toURI()).resolve("out").resolve("arabic-4.pdf").toFile();
        output.getParentFile().mkdirs();
//        output.getParentFile().deleteOnExit();
//        output.deleteOnExit();
        try (
            final PdfDocument document = new PdfDocument(
                new PdfReader(input),
                new PdfWriter(output)
            )
        ) {
            final int numberOfPages = document.getNumberOfPages();
            for (int pageNumber = 1; pageNumber <= numberOfPages; pageNumber++) {
                PdfPage page = document.getPage(pageNumber);
                PdfDictionary dict = page.getPdfObject();

                PdfObject object = dict.get(PdfName.Contents);
                if (object instanceof PdfStream) {
                    PdfStream stream = (PdfStream) object;
                    System.out.println("Page ".concat(String.valueOf(pageNumber)).concat(" :"));
                    byte[] data = stream.getBytes();
                    final String original = new String(data, StandardCharsets.UTF_8);
                    System.out.println("ORIGINAL:\n".concat(original));
                    final String replaced = original.replace("Hello World", "HELLO WORLD");
                    System.out.println("REPLACED:\n".concat(replaced));
                    stream.setData(replaced.getBytes(StandardCharsets.UTF_8));
                }
            }
        }
    }
}
