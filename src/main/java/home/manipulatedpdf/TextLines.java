package home.manipulatedpdf;

import org.apache.pdfbox.pdmodel.PDDocument;

import java.io.IOException;
import java.util.List;

final class TextLines {
    private final PDDocument document;
    private final ExtractionEngine extractionEngine;

    TextLines(final PDDocument document, final ExtractionEngine extractionEngine) {
        this.document = document;
        this.extractionEngine = extractionEngine;
    }

    List<TextLine> fromPage(int index) throws IOException {
        return this.extractionEngine.textLinesOf(this.document, index);
    }
}
