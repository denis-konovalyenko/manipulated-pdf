package home.manipulatedpdf;

import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.text.TextPosition;

import java.util.List;

final class TextLine {
    private final StringBuilder textBuilder;
    private final List<TextPosition> positions;
    private final PDRectangle cropBox;

    TextLine(final StringBuilder textBuilder, final List<TextPosition> positions, final PDRectangle cropBox) {
        this.textBuilder = textBuilder;
        this.positions = positions;
        this.cropBox = cropBox;
    }

    TextLine(final String text, final List<TextPosition> positions, final PDRectangle cropBox) {
        this(new StringBuilder(text), positions, cropBox);
    }

    String text() {
        return this.textBuilder.toString();
    }

    void appendText(final String text) {
        this.textBuilder.append(text);
    }

    void addPositions(final List<TextPosition> positions) {
        this.positions.addAll(positions);
    }

    /**
     * @todo Implement caching
     */
    Coordinates coordinates() {
        return new Coordinates(this.positions, this.cropBox);
    }

    /**
     * @todo Implement caching
     */
    Dimensions dimensions() {
        return new Dimensions(this.coordinates());
    }

    PDFont font() {
        return this.positions.get(this.positions.size() - 1).getFont();
    }

    float fontSize() {
        return this.positions.get(this.positions.size() - 1).getFontSize();
    }

    static final class Coordinates {
        private static final float MINIMUM = 0;

        private final List<TextPosition> textPositions;
        private final PDRectangle cropBox;

        Coordinates(final List<TextPosition> textPositions, final PDRectangle cropBox) {
            this.textPositions = textPositions;
            this.cropBox = cropBox;
        }

        /**
         * @todo Implement caching
         */
        float leftX() {
            final float x = this.textPositions.stream()
                .map(tp -> tp.getTextMatrix().getTranslateX())
                .min(Float::compareTo)
                .orElse(MINIMUM);
            return x + this.cropBox.getLowerLeftX();
        }

        float lowerY() {
            return this.textPositions.isEmpty()
                ? MINIMUM
                : this.textPositions.get(0).getTextMatrix().getTranslateY() + cropBox.getLowerLeftY();
        }

        /**
         * @todo Implement caching
         */
        float rightX() {
            final float x = this.textPositions.stream()
                .map(tp -> tp.getTextMatrix().getTranslateX())
                .max(Float::compareTo)
                .orElse(MINIMUM);
            return this.textPositions.isEmpty()
                ? x
                : x + this.textPositions.get(this.textPositions.size() - 1).getWidth();
        }

        float upperY() {
            return this.textPositions.isEmpty()
                ? MINIMUM
                : this.lowerY() + this.textPositions.get(0).getHeight();
        }
//
//        float extendedUpperY() {
//            return this.textPositions.isEmpty()
//                    ? MINIMUM
//                    : this.textPositions.get(0).getTextMatrix().getTranslateY()
//                        + this.textPositions.get(0).getFont().getBoundingBox().getHeight()
//        }
    }

    static final class Dimensions {
        private final Coordinates coordinates;

        Dimensions(final Coordinates coordinates) {
            this.coordinates = coordinates;
        }

        float width() {
            return this.coordinates.rightX() - this.coordinates.leftX();
        }

        float height() {
            return this.coordinates.upperY() - this.coordinates.lowerY();
        }
    }
}
