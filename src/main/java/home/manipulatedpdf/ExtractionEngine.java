package home.manipulatedpdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

final class ExtractionEngine extends PDFTextStripper {
    private PDDocument document;
    private int pageIndex;
    private List<TextLine> lines = null;
    private boolean startOfLine = true;

    ExtractionEngine() throws IOException {
    }

    List<TextLine> textLinesOf(final PDDocument document, int pageIndex) throws IOException {
        this.pageIndex = pageIndex;
        this.setStartPage(pageIndex + 1); // 1-based
        this.setEndPage(pageIndex + 1);
        this.getText(document);
        return this.lines;
    }

    @Override
    protected void startPage(PDPage page) throws IOException {
        startOfLine = true;
        super.startPage(page);
    }

    @Override
    protected void writeLineSeparator() throws IOException {
        startOfLine = true;
        super.writeLineSeparator();
    }

    @Override
    public String getText(PDDocument doc) throws IOException {
        this.document = doc;
        lines = new ArrayList<>();
        return super.getText(doc);
    }

    @Override
    protected void writeWordSeparator() throws IOException {
        lines.get(lines.size() - 1).appendText(getWordSeparator());
        super.writeWordSeparator();
    }

    @Override
    protected void writeString(String text, List<TextPosition> textPositions) throws IOException {
        if (startOfLine) {
            lines.add(new TextLine(text, textPositions, this.document.getPage(this.pageIndex).getCropBox()));
        } else {
            final TextLine line = lines.get(lines.size() - 1);
            line.appendText(text);
            line.addPositions(textPositions);
        }
        if (startOfLine) {
            startOfLine = false;
        }
        super.writeString(text, textPositions);
    }
}
